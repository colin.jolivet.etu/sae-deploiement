Lien dépôt : https://gitlab.univ-lille.fr/colin.jolivet.etu/sae-deploiement.git

Membres du groupe :

- Khatri Mounir
- Jolivet Colin
- Czepyga Maxime

Groupe H

# Rendu 1 Saé 3.03 Déploiement application
## Mise en place

### 1. Connexion à distance

#### a. Première connexion 

Nous allons commencer par tenter une première conexion **ssh** avec la machine attitrée :

    ssh frene18.iutinfo.fr

Par mesure de sécurité, une emprunte nous est montré afin de vérifier que le serveur ou la machine sur laquelle nous nous connectons est bien celle à laquelle nous nous attendons. 

Dans le cas de l'exemple (frene18) l'emprunte est : 

    rlpbRtluVcF9qLDPXzE/sCL6T5MDj1RTjh/k3Wzg3KA

Il ne nous reste donc qu'à la comparer à celle affichée sur l'écran. 

#### b. Faciliter la connexion 

Pour éviter d'avoir à renseigner son mot de passe à chaque nouvelle connexion SSH, il est intéressant d'utiliser un système cryptographique de **clé publique et privé**. 

Ainsi, le mot de passe ne nous sera demandé qu'une fois lors du démarrage de la machine. 

Il faut dans un premier temps générer la **clé publique** grâce à la commande :

    ssh-keygen

De cette manière, nous avons généré la clé dans le dossier :

    /home/infoetu/maxime.czepyga.etu/.ssh/id_rsa

Nous pouvons également vérifier son contenu grâce à la commande :

    cat $HOME/.ssh/id_rsa.pub

Nous devons maintenant **diffuser cette clé**, nous pouvons consulter la page de manuel :

    man ssh-copy-id 

Nous obtenons :

    ssh-copy-id [-f] [-n] [-s] [-i [identity_file]] [-p port]
    [-o ssh_option] [user@]hostname

Nous constatons alors que l'option -i nous permet de **spécifier le chemin de la clé générée**. De plus, la commande requiert le user **hostname**, autrement dit, le nom d'utilisateur de la machine qui va recevoir la clé. 

Nous nous rendons donc dans le dossier contenant la clé générée et éxécutons la commande :

    cd /home/infoetu/maxime.czepyga.etu/.ssh/
    ssh-copy-id -i id_rsa.pub maxime.czepyga.etu@frene18

### 2. Créer et gérer les machines virtuelles

Une fois la liaison ssh établie, nous pouvons spécifier les **sources d'environnement** :

    source /home/public/vm/vm.env

#### a. Création d’une machine virtuelle

Ensuite, nous pouvons créer la **machine virtuelle Matrix** sur la machine de virtualisation puis afficher la liste des machines virtuelles déjà créées afin de voir si elle est bien présente :

    vmiut creer matrix
    vmiut lister

#### b. Demmarage d’une machine virtuelle

Maintenant que la machine est créée, nous pouvons la démarrer :

    vmiut demarrer matrix

#### c. Arrêt / Suppression d’une machine virtuelle

Nous pouvons aussi arrêter la machine, la supprimer ou voir ses informations grâce aux lignes de commandes suivantes :

    vmiut arreter matrix
    vmiut supprimer matrix
    vmiut info matrix

#### d. Utilisation de la machine virtuelle

Nous voudrions avoir maintenant un affichage **console**. Pour cela nous utilisons cette ligne de commande depuis notre machine de virtualisation :

    vmiut console matrix

Seulement la sortie vidéo est par défaut sur la machine de virtualisation, hors nous voudrions l'avoir sur la machine physique.

Nous devons donc fermer la connexion SSH avec la machine de virtualisation 

    exit

et **rediriger la sortie vidéo** depuis la machine physique.

    ssh -X frene18

Une fois cette manipulation effectuée, nous pouvons nous reconnecter à la machine de virtualisation, réintégrer les sources, redémarrer la machine virtuelle puis retaper la commande :

    vmiut console matrix

Depuis la console, on peut se connecter à la machine virtuelle en tant que **root**, simplement avec l'identifiant et mot de passe : root root.

On peut alors voir qu'elle est l'adresse ip de la machine et afficher sa table de routage avec les commandes :

    ip addr show
    ip route show

### 3. Configurer et mettre à jour la machine virtuelle

Maintenant qu'on s'est connecté en tant que **root** à la machine virtuelle via la console, on peut se connecter en tant que **user via ssh** :

    ssh user@192.168.194.26

Le mot de passe dans ce cas est user.

#### a. Connexion root et SSH

Etant donné qu'on a établi une connexion ssh sur le compte user, on peut se demander si une connexion **ssh** sur le compte **root** est possible? Essayons :

    ssh root@192.168.194.26

La connexion nous est refusée. Il serait dangereux de pouvoir s'attribuer les droits root sur une machine à distance. 

Après avoir lu la page du manuel :

    man su

On se rend compte que l'option **--login** nous permet d'avoir un **environnement** similaire  à celui que nous aurions obtenu si nous nous étions **connectés directement** avec le login d'un autre utilisateur.

Par défaut, si on ne fournit pas d'option  à la commande su, on tente une connexion en tant que super utilisateur, **root**.

Il ne nous reste alors qu'à entrer le mot de passe.

#### b. Connexion root et SSH

La machine virtuelle, en l'étât est dans un **réseau privé**, ce qui signifie que depuis cette machine, nous n'avons aucun accès à internet. Ainsi, la commande 

    wget https://www.framasoft.org

n'aboutit à rien.

Pour avoir accès à internet depuis la machine virtuelle, nous devons **ajouter le proxy** afin qu'il agisse comme un intermédiaire entre le réseau privé et le monde extérieur.

Pour cela, il nous faut modifier le fichier :

    /etc/environnement

Et ajouter les lignes :

    HTTP_PROXY=http://cache.univ-lille.fr:3128
    HTTPS_PROXY=http://cache.univ-lille.fr:3128
    http_proxy=http://cache.univ-lille.fr:3128
    https_proxy=http://cache.univ-lille.fr:3128
    NO_PROXY=localhost,192.168.194.0/24,172.18.48.0/22

Nous pouvons alors éxécuter la commande :

    wget https://www.framasoft.org

#### c. Mise à jour

A présent, nous devrions **mettre la machine virtuelle à jour**. Pour cela, on se connecte en temps que root et on exécute la commande :

    apt update && apt full-upgrade

Comme pour n'importe quelle mise à jour, nous **redémarrons** ensuite la machine :

    reboot

#### d. Installation d’outils

Il serait alors intéressant d'installer quelques **logiciels** comme vim, less, tree et rsync :

    apt-get install vim
    apt-get install less
    apt-get install tree
    apt-get install rsync

### 5. Quelques trucs en plus

Nous pouvons maintenant configurer des **alias** pour accéder à la machine virtuelle directement depuis la machine physique sans passer par la machine de virtualisation.

Pour cela, il faut **modifier le fichier**

    ~/.ssh/config

et saisir les alias comme :

    Host virt
        Hostname frene18.iutinfo.fr

et 
    
    Host vm
        User user
        Hostname 192.168.194.3

et 

    Host vmjump
        User user
        Hostname 192.168.194.3
        ProxyJump virt

