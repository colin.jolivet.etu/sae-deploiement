Lien dépôt : https://gitlab.univ-lille.fr/colin.jolivet.etu/sae-deploiement.git

Membres du groupe :

- Khatri Mounir
- Jolivet Colin
- Czepyga Maxime

Groupe H

# Fin de configuration et mise en place d’un premier service

## 1. Dernières configurations sur la VM

### a. Changement du nom de machine 

Pour changer le nom de la vm, nous nous rendons dans le fichier :

    nano /etc/hostname

et nous remplaçons le nom par *matrix*.

Pour modifier notre nom aux yeux des autres machines, nous faisons la même modification en changeant le nom de la machine virtuelle dans le paragraphe **auto-generates hostnames** :

    nano /etc/hosts

Nous pouvons donc vérifier que nos modifications sont effectives grâce à la commande **ping** :

    ping matrix

### b. Installation et configuration de la commande sudo

Il serait maintenant intéressant de configurer la commande **sudo** afin d'attribuer certains droits administrateurs à des utilisateurs particuliers.

En effet la commande sudo permet à certains utilisateurs d'éffectuer des actions normalement autorisées qu'aux administrateurs.

Dans un premier temps, on installe la commande **sudo** :

    apt get-instal sudo  

on peut alors lire la page du manuel :

    man sudoers

ensuite, on peut consulter le fichier :

    namo /etc/sudoers

On ajoute donc l'utilisateur au **groupe sudo** avec la commande :

    sudo username -aG sudo user*

### c. Configuration de la synchronisation d’horloge

Pour renforcer la sécurité, il serait intéressant de synchroniser l'horloge avec le serveur via le **protocole NTP**.

En observant la page du manuel

    man date

on voit :

    date [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]

On est donc en mesure de voir la date du jour grâce à la commande **date**.

En consultant la page du manuel 

    man journalctl

on obtient :

    journalctl - Query the systemd journal
    ...
    -u, --unit=UNIT|PATTERN
           Show messages for the specified systemd unit UNI, or for any of the units matched by PATTERN.

On remarque alors, que l'option **-u**, nous permet de spécifier le service qui nous intéresse. 

Pour voir donc les messages spécifiques au **unit service systemd-timesyncd**, nous exécutons la commande :

    journalctl -u systemd-timesyncd

Nous pursuivons alors en lisant les pages de manuel suivantes :

    man timesyncd.conf
    man timedatectl
    man systemctl

Maintenant, pour synchroniser l'heure de la machine avec le serveur NTP de l'université, nous modifions le fichier :

    nano /etc/systemd/timesyncd.conf

Nous retirons les commentaires *“NTP=[ajouter serv]"*.

Puis, nous exécutons la commande :

    timedatectl set-ntp yes

Enfin, nous redémarrons le **service** avec la commande :
 
    systemctl restart systemd-timesyncd.service

## 2. Installation et configuration basique d’un serveur de base de données

Pour installer **PostgreSQL**, nous commençons par nous connecter à la machine virtuelle en tant qu'utilisateur **root**.

Ensuite, nous exécutons la commande suivante :

    apt install postgresql

Nous vérifions alors que le service est bien **activé** et **démarré** avec la commande :

    systemctl status postgres

D'après le manuel

    man createuser

la commande **createuser** nous permet de créer un nouvel utilisateur postgresql :

    createuser [connection-option...] [option...] [username]

Il nous faut simplement spécifier son **nom** dans les **options** :

    createuser matrix

Nous entrons alors le mot de passe associé à l'utilisateur avec la commande :

psql -c "ALTER USER [matrix] WITH password ‘[matrix]’"

Ensuite, nous consultons le manuel pour savoir comment créer une **database** via l'utilisateur matrix :

    man createdb

Nous obtenons :

    createdb [connection-option...] [option...] [dbname [description]]

Il faut donc préciser le **nom de la base** de données et **l'utilisateur** origine :

    createdb matrix -O matrix

Nous pouvons maintanant nous connecter à la database dans un shell de l'utilisateur user :

    psql -H localhost -U matrix

On entre ensuite le mot de passe *matrix*.

Ansi, nous pouvons, créer des tables, insérer des données, les modifier..

    create table test(tno int, value text);
    insert into test values (1, 'test insertion');
    select * from test;